-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: bivaalekari
-- ------------------------------------------------------
-- Server version	5.7.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pacient`
--

DROP TABLE IF EXISTS `pacient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pacient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jmeno` varchar(255) CHARACTER SET utf8 NOT NULL,
  `datumNarozeni` date NOT NULL,
  `adresa` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `telefon` varchar(30) CHARACTER SET utf8 NOT NULL,
  `je_aktivni` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacient`
--

LOCK TABLES `pacient` WRITE;
/*!40000 ALTER TABLE `pacient` DISABLE KEYS */;
INSERT INTO `pacient` VALUES (1,'Petr Rychlý','1978-02-03','Lesnická 34, Praha 4','petrrychly@email.com','238323323',_binary '\0'),(3,'Milada Nováková','1945-05-23','Kounicova 2, Praha 7','miladan@centrum.cz','785784232',_binary ''),(4,'Jan Hodný','1989-12-07','Metro 392, Praha 4','hodnyj@email.com','772881992',_binary ''),(5,'Pavel Kotek','1999-05-12','Metrážní 2, Praha 5','pavelkotek2@email.cz','789345643',_binary '\0');
/*!40000 ALTER TABLE `pacient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prohlidka`
--

DROP TABLE IF EXISTS `prohlidka`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prohlidka` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(120) CHARACTER SET utf8 NOT NULL,
  `popis` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prohlidka`
--

LOCK TABLES `prohlidka` WRITE;
/*!40000 ALTER TABLE `prohlidka` DISABLE KEYS */;
INSERT INTO `prohlidka` VALUES (1,'Vstupní','Klasická vstupní prohlídka pri nástupu zamestnance do partnerské firmy'),(2,'Periodická','Opakující se prohlídka obvykle jednou za 2 roky, při které je provedena veškerá nutná prevence'),(3,'Ambulance','Nutná ambulantní prohlídka v případě náhlých potíží');
/*!40000 ALTER TABLE `prohlidka` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rezervace`
--

DROP TABLE IF EXISTS `rezervace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rezervace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPacient` int(11) DEFAULT NULL,
  `idProhlidka` int(11) DEFAULT NULL,
  `termin` datetime DEFAULT NULL,
  `poznamka` text,
  PRIMARY KEY (`id`),
  KEY `pacient_idx` (`idPacient`),
  KEY `prohlidka_idx` (`idProhlidka`),
  CONSTRAINT `pacient` FOREIGN KEY (`idPacient`) REFERENCES `pacient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `prohlidka` FOREIGN KEY (`idProhlidka`) REFERENCES `prohlidka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rezervace`
--

LOCK TABLES `rezervace` WRITE;
/*!40000 ALTER TABLE `rezervace` DISABLE KEYS */;
INSERT INTO `rezervace` VALUES (1,5,1,'2022-03-16 00:00:00','Nutno vysetrit kolena'),(4,3,2,'2022-02-24 00:00:00','Nic'),(5,4,1,'2022-03-08 00:00:00','Málo vápníku, pán volal predem a prosí o recept na léky');
/*!40000 ALTER TABLE `rezervace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uzivatel`
--

DROP TABLE IF EXISTS `uzivatel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `uzivatel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) CHARACTER SET utf8 NOT NULL,
  `jmeno` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `heslo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `je_aktivni` bit(1) NOT NULL DEFAULT b'1',
  `je_admin` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uzivatel`
--

LOCK TABLES `uzivatel` WRITE;
/*!40000 ALTER TABLE `uzivatel` DISABLE KEYS */;
INSERT INTO `uzivatel` VALUES (1,'admin','admin','janadmin@email.com','test',_binary '',_binary ''),(2,'petra','Petra Veliká','petravelika@email.com','heslo',_binary '',_binary '\0');
/*!40000 ALTER TABLE `uzivatel` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-09 15:54:09
