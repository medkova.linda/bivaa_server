﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace bivaa_server.Controllers
{
    public class ProhlidkaController : ApiController
    {
        // GET: api/Prohlidka
        public HttpResponseMessage Get()
        {
            List<Prohlidka> result;
            using (var db = new AppDbModel())
            {
                var prohlidky = from prohlidka in db.prohlidky select prohlidka;
                result = prohlidky.OrderBy(o => o.nazev).ToList();
                var jsonString = JsonConvert.SerializeObject(result);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // GET: api/Prohlidka/id
        public HttpResponseMessage Get(int id)
        {
            Prohlidka prohlidka;
            using (var db = new AppDbModel())
            {
                prohlidka = (from p in db.prohlidky where p.id == id select p).SingleOrDefault();
                var jsonString = JsonConvert.SerializeObject(prohlidka);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // POST: api/Prohlidka
        public void Post(HttpRequestMessage value)
        {
            var prohlidka = JsonConvert.DeserializeObject<Prohlidka>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                db.prohlidky.Add(prohlidka);
                db.SaveChanges();
            }
        }

        // PUT: api/Prohlidka/id
        public void Put(int id, HttpRequestMessage value)
        {
            var requestProhlidka = JsonConvert.DeserializeObject<Prohlidka>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                var prohlidka = (from r in db.prohlidky where r.id == id select r).SingleOrDefault();
                prohlidka.nazev = requestProhlidka.nazev;
                prohlidka.popis = requestProhlidka.popis;
                db.SaveChanges();
            }
        }

        // DELETE: api/Prohlidka/id
        public void Delete(int id)
        {
            using (var db = new AppDbModel())
            {
                var prohlidka = (from p in db.prohlidky where p.id == id select p).SingleOrDefault();
                db.prohlidky.Remove(prohlidka);
                db.SaveChanges();
            }
        }
    }
}
