﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;

namespace bivaa_server.Controllers
{
    public class PacientController : ApiController
    {
        // GET: api/Pacient
        public HttpResponseMessage Get()
        {
            List<Pacient> result;
            using (var db = new AppDbModel())
            {
                var pacienti = from pacient in db.pacienti select pacient;
                result = pacienti.OrderBy(o => o.jmeno).ToList();
                var jsonString = JsonConvert.SerializeObject(result);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // GET: api/Pacient/id
        public HttpResponseMessage Get(int id)
        {
            Pacient pacient;
            using (var db = new AppDbModel())
            {
                pacient = (from p in db.pacienti where p.id == id select p).SingleOrDefault();
                var jsonString = JsonConvert.SerializeObject(pacient);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // POST: api/Pacient
        public void Post(HttpRequestMessage value)
        {
            var pacient = JsonConvert.DeserializeObject<Pacient>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                db.pacienti.Add(pacient);
                db.SaveChanges();
            }
        }

        // PUT: api/Pacient/id
        public void Put(int id, HttpRequestMessage value)
        {
            var requestPacient = JsonConvert.DeserializeObject<Pacient>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                var pacient = (from r in db.pacienti where r.id == id select r).SingleOrDefault();
                pacient.je_aktivni = requestPacient.je_aktivni;
                pacient.jmeno = requestPacient.jmeno;
                pacient.email = requestPacient.email;
                pacient.telefon = requestPacient.telefon;
                pacient.adresa = requestPacient.adresa;
                pacient.datumNarozeni = requestPacient.datumNarozeni;
                db.SaveChanges();
            }
        }

        // DELETE: api/Pacient/id
        public void Delete(int id)
        {
            using (var db = new AppDbModel())
            {
                var pacient = (from p in db.pacienti where p.id == id select p).SingleOrDefault();
                db.pacienti.Remove(pacient);
                db.SaveChanges();
            }
        }
    }
}
