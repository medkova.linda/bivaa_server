﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace bivaa_server.Controllers
{
    public class BaseController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("Hello")
            };
            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            return resp;
        }
    }
}
