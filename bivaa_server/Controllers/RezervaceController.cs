﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace bivaa_server.Controllers
{
    public class RezervaceController : ApiController
    {
        // GET: api/Rezervace
        public HttpResponseMessage Get()
        {
            List<Rezervace> result;
            using (var db = new AppDbModel())
            {
                var rezervace = from rez in db.rezervace select rez;
                result = rezervace.OrderBy(o => o.termin).ToList();
                var jsonString = JsonConvert.SerializeObject(result);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // GET: api/Rezervace/id
        public HttpResponseMessage Get(int id)
        {
            Rezervace rezervace;
            using (var db = new AppDbModel())
            {
                rezervace = (from p in db.rezervace where p.id == id select p).SingleOrDefault();
                var jsonString = JsonConvert.SerializeObject(rezervace);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // POST: api/Rezervace
        public void Post(HttpRequestMessage value)
        {
            var rezervace = JsonConvert.DeserializeObject<Rezervace>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                db.rezervace.Add(rezervace);
                db.SaveChanges();
            }
        }

        // PUT: api/Rezervace/id
        public void Put(int id, HttpRequestMessage value)
        {
            var requestRezervace = JsonConvert.DeserializeObject<Rezervace>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                var rezervace = (from r in db.rezervace where r.id == id select r).SingleOrDefault();
                rezervace.poznamka = requestRezervace.poznamka;
                rezervace.termin = requestRezervace.termin;
                rezervace.idPacient = requestRezervace.idPacient;
                rezervace.idProhlidka = requestRezervace.idProhlidka;
                db.SaveChanges();
            }
        }

        // DELETE: api/Rezervace/id
        public void Delete(int id)
        {
            using (var db = new AppDbModel())
            {
                var rezervace = (from p in db.rezervace where p.id == id select p).SingleOrDefault();
                db.rezervace.Remove(rezervace);
                db.SaveChanges();
            }
        }
    }
}
