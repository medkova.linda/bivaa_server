using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace bivaa_server
{
    public partial class AppDbModel : DbContext
    {
        public AppDbModel() : base("name=AppDbModel")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        
        public virtual DbSet<Uzivatel> uzivatele { get; set; }
        public virtual DbSet<Pacient> pacienti { get; set; }
        public virtual DbSet<Prohlidka> prohlidky { get; set; }
        public virtual DbSet<Rezervace> rezervace { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Uzivatel
            modelBuilder.Entity<Uzivatel>()
                .Property(e => e.login)
                .IsUnicode(false);

            modelBuilder.Entity<Uzivatel>()
                .Property(e => e.jmeno)
                .IsUnicode(false);

            modelBuilder.Entity<Uzivatel>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Uzivatel>()
                .Property(e => e.heslo)
                .IsUnicode(false);

            // Pacient
            modelBuilder.Entity<Pacient>()
                .Property(e => e.jmeno)
                .IsUnicode(false);

            modelBuilder.Entity<Pacient>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Pacient>()
                .Property(e => e.telefon)
                .IsUnicode(false);

            modelBuilder.Entity<Pacient>()
                .HasMany(e => e.rezervace)
                .WithRequired(e => e.Pacient)
                .HasForeignKey(e => e.idPacient)
                .WillCascadeOnDelete(false);

            // Rezervace
            modelBuilder.Entity<Rezervace>()
                .Property(e => e.poznamka)
                .IsUnicode(false);

            // Prohlidka
            modelBuilder.Entity<Prohlidka>()
                .Property(e => e.nazev)
                .IsUnicode(false);

            modelBuilder.Entity<Prohlidka>()
                .Property(e => e.popis)
                .IsUnicode(false);

            modelBuilder.Entity<Prohlidka>()
                .HasMany(e => e.rezervace)
                .WithRequired(e => e.Prohlidka)
                .HasForeignKey(e => e.idProhlidka)
                .WillCascadeOnDelete(false);
        }
    }
}
