using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace bivaa_server
{
    [Table("pacient")]
    public partial class Pacient
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Pacient()
        {
            rezervace = new HashSet<Rezervace>();
        }
        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string jmeno { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime datumNarozeni { get; set; }

        [Required]
        [StringLength(255)]
        public string adresa { get; set; }

        [Required]
        [StringLength(255)]
        public string email { get; set; }

        [Required]
        [StringLength(30)]
        public string telefon { get; set; }

        [Column(TypeName = "bit")]
        public bool je_aktivni { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rezervace> rezervace { get; set; }
    }
}