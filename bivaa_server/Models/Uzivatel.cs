using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace bivaa_server
{
    [Table("uzivatel")]
    public partial class Uzivatel
    {
        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string login { get; set; }

        [Required]
        [StringLength(255)]
        public string jmeno { get; set; }

        [Required]
        [StringLength(255)]
        public string email { get; set; }

        [Required]
        [StringLength(255)]
        public string heslo { get; set; }

        [Column(TypeName = "bit")]
        public bool je_aktivni { get; set; }

        [Column(TypeName = "bit")]
        public bool je_admin { get; set; }
    }
}
