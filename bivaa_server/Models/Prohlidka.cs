using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace bivaa_server
{
    [Table("prohlidka")]
    public partial class Prohlidka
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Prohlidka()
        {
            rezervace = new HashSet<Rezervace>();
        }
        public int id { get; set; }

        [Required]
        [StringLength(120)]
        public string nazev { get; set; }

        [Required]
        [StringLength(255)]
        public string popis { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rezervace> rezervace { get; set; }
    }
}
