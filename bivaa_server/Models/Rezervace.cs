using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace bivaa_server
{
    [Table("rezervace")]
    public partial class Rezervace
    {
        public int id { get; set; }

        public int idPacient { get; set; }

        public int idProhlidka { get; set; }

        [Column(TypeName = "date")]
        public DateTime termin { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string poznamka { get; set; }

        public virtual Pacient Pacient { get; set; }

        public virtual Prohlidka Prohlidka { get; set; }
    }
}